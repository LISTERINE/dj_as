from dj_arp_storm.callbacks import CallbackBase
class Callbacks(CallbackBase):

    @staticmethod
    def has_protocol(pkt, protocol):
        return protocol in map(lambda L: L.layer_name, pkt.layers)

    @staticmethod
    def has_flags(pkt, flags_as_int):
        if not Callbacks.has_protocol(pkt, "tcp"):
            return False
        return int(pkt.tcp.flags, 16) == flags_as_int

    @staticmethod
    def has_port(pkt, port, ptype="dstport"):
        if not Callbacks.has_protocol(pkt, "tcp"):
            return False
        return getattr(pkt.tcp, ptype) == str(port)




    def handshake_scale(self, pkt):
        while not self.has_flags(pkt, 2):
            pkt = yield
        pkt = yield self.sounds.play(self.sounds.sounds['violin']['b3.ogg'])
        while not self.has_flags(pkt, 18):
            pkt = yield
        pkt = yield self.sounds.delay_play(self.sounds.sounds['violin']['a3.ogg'], 0.2)
        while not int(pkt.tcp.flags, 16):
            pkt = yield
        pkt = yield self.sounds.delay_play(self.sounds.sounds['violin']['c-3.ogg'], 0.2)

    def ssh_drum(self, pkt):
        while not self.has_port(pkt, 22, ptype="dstport"):
            pkt = yield
        pkt = yield self.sounds.delay_play(self.sounds.sounds['drum']['snare-1.ogg'])
