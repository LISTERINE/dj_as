Sound pack downloaded from Freesound.org
----------------------------------------

This pack of sounds contains sounds by Snapper4298 ( http://www.freesound.org/people/Snapper4298/  )
You can find this pack online at: http://www.freesound.org/people/Snapper4298/packs/11176/


License details
---------------

Sampling+: http://creativecommons.org/licenses/sampling+/1.0/
Creative Commons 0: http://creativecommons.org/publicdomain/zero/1.0/
Attribution: http://creativecommons.org/licenses/by/3.0/
Attribution Noncommercial: http://creativecommons.org/licenses/by-nc/3.0/


Sounds in this pack
-------------------

  * 177507__snapper4298__snap-6.wav
    * url: http://www.freesound.org/people/Snapper4298/sounds/177507/
    * license: Attribution
  * 177506__snapper4298__snap-7.wav
    * url: http://www.freesound.org/people/Snapper4298/sounds/177506/
    * license: Attribution
  * 177505__snapper4298__snap-8.wav
    * url: http://www.freesound.org/people/Snapper4298/sounds/177505/
    * license: Attribution
  * 177504__snapper4298__snap-9.wav
    * url: http://www.freesound.org/people/Snapper4298/sounds/177504/
    * license: Attribution
  * 177503__snapper4298__snap-10.wav
    * url: http://www.freesound.org/people/Snapper4298/sounds/177503/
    * license: Attribution
  * 177502__snapper4298__snap-11.wav
    * url: http://www.freesound.org/people/Snapper4298/sounds/177502/
    * license: Attribution
  * 177501__snapper4298__snap-12.wav
    * url: http://www.freesound.org/people/Snapper4298/sounds/177501/
    * license: Attribution
  * 177500__snapper4298__snap-5.wav
    * url: http://www.freesound.org/people/Snapper4298/sounds/177500/
    * license: Attribution
  * 177497__snapper4298__snap-1234.wav
    * url: http://www.freesound.org/people/Snapper4298/sounds/177497/
    * license: Attribution
  * 177496__snapper4298__snap-1.wav
    * url: http://www.freesound.org/people/Snapper4298/sounds/177496/
    * license: Attribution
  * 177495__snapper4298__snap-2.wav
    * url: http://www.freesound.org/people/Snapper4298/sounds/177495/
    * license: Attribution
  * 177494__snapper4298__snap-3.wav
    * url: http://www.freesound.org/people/Snapper4298/sounds/177494/
    * license: Attribution

