Sound pack downloaded from Freesound.org
----------------------------------------

This pack of sounds contains sounds by ldk1609 ( http://www.freesound.org/people/ldk1609/  )
You can find this pack online at: http://www.freesound.org/people/ldk1609/packs/3578/


License details
---------------

Sampling+: http://creativecommons.org/licenses/sampling+/1.0/
Creative Commons 0: http://creativecommons.org/publicdomain/zero/1.0/
Attribution: http://creativecommons.org/licenses/by/3.0/
Attribution Noncommercial: http://creativecommons.org/licenses/by-nc/3.0/


Sounds in this pack
-------------------

  * 56228__ldk1609__violin_spiccato_G5.aif
    * url: http://www.freesound.org/people/ldk1609/sounds/56228/
    * license: Creative Commons 0
  * 56227__ldk1609__violin_spiccato_G4.aif
    * url: http://www.freesound.org/people/ldk1609/sounds/56227/
    * license: Creative Commons 0
  * 56226__ldk1609__violin_spiccato_G3.aif
    * url: http://www.freesound.org/people/ldk1609/sounds/56226/
    * license: Creative Commons 0
  * 56225__ldk1609__violin_spiccato_G2.aif
    * url: http://www.freesound.org/people/ldk1609/sounds/56225/
    * license: Creative Commons 0
  * 56224__ldk1609__violin_spiccato_G_5.aif
    * url: http://www.freesound.org/people/ldk1609/sounds/56224/
    * license: Creative Commons 0
  * 56223__ldk1609__violin_spiccato_G_4.aif
    * url: http://www.freesound.org/people/ldk1609/sounds/56223/
    * license: Creative Commons 0
  * 56222__ldk1609__violin_spiccato_G_3.aif
    * url: http://www.freesound.org/people/ldk1609/sounds/56222/
    * license: Creative Commons 0
  * 56221__ldk1609__violin_spiccato_G_2.aif
    * url: http://www.freesound.org/people/ldk1609/sounds/56221/
    * license: Creative Commons 0
  * 56220__ldk1609__violin_spiccato_F5.aif
    * url: http://www.freesound.org/people/ldk1609/sounds/56220/
    * license: Creative Commons 0
  * 56219__ldk1609__violin_spiccato_F4.aif
    * url: http://www.freesound.org/people/ldk1609/sounds/56219/
    * license: Creative Commons 0
  * 56218__ldk1609__violin_spiccato_F3.aif
    * url: http://www.freesound.org/people/ldk1609/sounds/56218/
    * license: Creative Commons 0
  * 56217__ldk1609__violin_spiccato_F_5.aif
    * url: http://www.freesound.org/people/ldk1609/sounds/56217/
    * license: Creative Commons 0
  * 56216__ldk1609__violin_spiccato_F_4.aif
    * url: http://www.freesound.org/people/ldk1609/sounds/56216/
    * license: Creative Commons 0
  * 56215__ldk1609__violin_spiccato_F_3.aif
    * url: http://www.freesound.org/people/ldk1609/sounds/56215/
    * license: Creative Commons 0
  * 56214__ldk1609__violin_spiccato_E5.aif
    * url: http://www.freesound.org/people/ldk1609/sounds/56214/
    * license: Creative Commons 0
  * 56213__ldk1609__violin_spiccato_E4.aif
    * url: http://www.freesound.org/people/ldk1609/sounds/56213/
    * license: Creative Commons 0
  * 56212__ldk1609__violin_spiccato_E3.aif
    * url: http://www.freesound.org/people/ldk1609/sounds/56212/
    * license: Creative Commons 0
  * 56211__ldk1609__violin_spiccato_D5.aif
    * url: http://www.freesound.org/people/ldk1609/sounds/56211/
    * license: Creative Commons 0
  * 56210__ldk1609__violin_spiccato_D4.aif
    * url: http://www.freesound.org/people/ldk1609/sounds/56210/
    * license: Creative Commons 0
  * 56209__ldk1609__violin_spiccato_D3.aif
    * url: http://www.freesound.org/people/ldk1609/sounds/56209/
    * license: Creative Commons 0
  * 56208__ldk1609__violin_spiccato_D_5.aif
    * url: http://www.freesound.org/people/ldk1609/sounds/56208/
    * license: Creative Commons 0
  * 56207__ldk1609__violin_spiccato_D_4.aif
    * url: http://www.freesound.org/people/ldk1609/sounds/56207/
    * license: Creative Commons 0
  * 56206__ldk1609__violin_spiccato_D_3.aif
    * url: http://www.freesound.org/people/ldk1609/sounds/56206/
    * license: Creative Commons 0
  * 56205__ldk1609__violin_spiccato_C5.aif
    * url: http://www.freesound.org/people/ldk1609/sounds/56205/
    * license: Creative Commons 0
  * 56204__ldk1609__violin_spiccato_C4.aif
    * url: http://www.freesound.org/people/ldk1609/sounds/56204/
    * license: Creative Commons 0
  * 56203__ldk1609__violin_spiccato_C3.aif
    * url: http://www.freesound.org/people/ldk1609/sounds/56203/
    * license: Creative Commons 0
  * 56202__ldk1609__violin_spiccato_C_5.aif
    * url: http://www.freesound.org/people/ldk1609/sounds/56202/
    * license: Creative Commons 0
  * 56201__ldk1609__violin_spiccato_C_4.aif
    * url: http://www.freesound.org/people/ldk1609/sounds/56201/
    * license: Creative Commons 0
  * 56200__ldk1609__violin_spiccato_C_3.aif
    * url: http://www.freesound.org/people/ldk1609/sounds/56200/
    * license: Creative Commons 0
  * 56199__ldk1609__violin_spiccato_B5.aif
    * url: http://www.freesound.org/people/ldk1609/sounds/56199/
    * license: Creative Commons 0
  * 56198__ldk1609__violin_spiccato_B4.aif
    * url: http://www.freesound.org/people/ldk1609/sounds/56198/
    * license: Creative Commons 0
  * 56197__ldk1609__violin_spiccato_B3.aif
    * url: http://www.freesound.org/people/ldk1609/sounds/56197/
    * license: Creative Commons 0
  * 56196__ldk1609__violin_spiccato_B2.aif
    * url: http://www.freesound.org/people/ldk1609/sounds/56196/
    * license: Creative Commons 0
  * 56195__ldk1609__violin_spiccato_A5.aif
    * url: http://www.freesound.org/people/ldk1609/sounds/56195/
    * license: Creative Commons 0
  * 56194__ldk1609__violin_spiccato_A4.aif
    * url: http://www.freesound.org/people/ldk1609/sounds/56194/
    * license: Creative Commons 0
  * 56193__ldk1609__violin_spiccato_A3.aif
    * url: http://www.freesound.org/people/ldk1609/sounds/56193/
    * license: Creative Commons 0
  * 56192__ldk1609__violin_spiccato_A2.aif
    * url: http://www.freesound.org/people/ldk1609/sounds/56192/
    * license: Creative Commons 0
  * 56191__ldk1609__violin_spiccato_A_5.aif
    * url: http://www.freesound.org/people/ldk1609/sounds/56191/
    * license: Creative Commons 0
  * 56190__ldk1609__violin_spiccato_A_4.aif
    * url: http://www.freesound.org/people/ldk1609/sounds/56190/
    * license: Creative Commons 0
  * 56189__ldk1609__violin_spiccato_A_3.aif
    * url: http://www.freesound.org/people/ldk1609/sounds/56189/
    * license: Creative Commons 0
  * 56188__ldk1609__violin_spiccato_A_2.aif
    * url: http://www.freesound.org/people/ldk1609/sounds/56188/
    * license: Creative Commons 0

